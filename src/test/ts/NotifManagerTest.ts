// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection } from "@yinz/commons.data/ts/DataSource";

import {promisify} from "util";
import NotifManager, { NotifPostOptions } from "main/ts/NotifManager";
import { Logger } from "@yinz/commons";
import Notif from "@yinz/notif.data/models/Notif";
import ArchNotif from "@yinz/notif.data/models/ArchNotif";
import INotifProcessor from "../../main/ts/INotifProcessor";
import Templ from "@yinz/notif.data/models/Templ";
import I18nTempl from "@yinz/notif.data/models/I18nTempl";

const wait = promisify(setTimeout);


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/notif.data/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');
const notifDao = container.getBean<TypeormDao<Notif>>('notifDao');
const templDao = container.getBean<TypeormDao<Templ>>('templDao');
const i18nTemplDao = container.getBean<TypeormDao<I18nTempl>>('i18nTemplDao');
const archNotifDao = container.getBean<TypeormDao<ArchNotif>>('archNotifDao');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');
const notifManager = container.getBean<NotifManager>('notifManager');
const logger = container.getBean<Logger>('logger');

class SomeNotifProcessor implements INotifProcessor {

    async process(notif: Notif, message: string, subject: string): Promise<void> {
        return;
    }

}
let someNotifProcessor = new SomeNotifProcessor();

notifManager.subscribe('I', someNotifProcessor)

let conn: YinzConnection;
// let trans: YinzTransaction;


describe('| notif.data.NotifManager',  () => {

    let templs: Templ[] = [        
        { code: 'WELCOME',            message: 'Welcome ,{{=model.name}}!' },
        { code: 'WELCOME_SUBJECT', message: 'Welcome ,{{=model.name}}!' },
        { code: 'WELCOME_WITH_MODEL_TITLE', message: 'Welcome, {{=model.name}}!' },        
        { code: 'WELCOME_WITH_MODEL_SUBJECT', message: 'Welcome, {{=model.name}}!' },        
    ] as Templ[];

    // let i18nTempls: I18nTempl[] = [
    //     { locale: 'en', message: 'Welcome {{model.name}}!'},
    //     { locale: 'fr', message: 'Bienvenu {{model.name}}!'},
    // ] as I18nTempl[];

    



    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();

            await templDao.deleteByFilter(conn, { id: { op: ">", val: -1 } }, { user: "__super_user__" })
            await i18nTemplDao.deleteByFilter(conn, { id: { op: ">", val: -1 } }, { user: "__super_user__" })
            templs = await templDao.createAll(conn, templs, {user: "__super_user__"});
            
            // await i18nTemplDao.createAll(conn, i18nTempls, {user: "__super_user__"});

            // do not wait for manager
            notifManager.startup(conn, {logger, pollingTimer: 3000} );

            // give some time for manager to startup
            await wait(1000)

            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {

            // shutdown manager
            await notifManager.shutdown();
            await notifManager.release();

            // wait for manager to shutdown
            await wait(5000)

            await dataSource.relConn(conn)            

            

            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {            
            await notifDao.deleteByFilter(conn, {id: {op: ">", val: -1}}, {user: "__super_user__"})
            await archNotifDao.deleteByFilter(conn, { id: { op: ">", val: -1 } }, { user: "__super_user__" })
            await i18nTemplDao.deleteByFilter(conn, { id: { op: ">", val: -1 } }, { user: "__super_user__" })
            resolve(true);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await notifDao.deleteByFilter(conn, { id: { op: ">", val: -1 } }, { user: "__super_user__" })
            await archNotifDao.deleteByFilter(conn, { id: { op: ">", val: -1 } }, { user: "__super_user__" })
            await i18nTemplDao.deleteByFilter(conn, { id: { op: ">", val: -1 } }, { user: "__super_user__" })
            resolve(true);
        })
    });

    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is ready \n' +
        '       | - model not supplied \n' +
        '       | Expected :  \n' +
        '       | -> forward the message to a NotifProcessor  \n' +
        '       | -> archive the notif  \n' +
        '       | -> use message as provided  \n' 
        , async () => {

        // 1. prepare test-data 

        let notif = new Notif();
        notif.ref = chance.string({ length: 10 });
        notif.dest = chance.string({ length: 10 });
        notif.destType = "I"
        notif.readyOn = new Date();
        notif.reason = chance.string({ length: 10 });        
        notif.message = chance.string({length: 100})

        let options: NotifPostOptions = {
            user: "__super_user__"
        }

        // 2. execute test
        await notifManager.post(conn, notif, options);

        // 3. assert result 
        //    give some time for the notif to be processed
        await wait(500);

        // 3.1. check that notif was deleted from `notif` table 
        let notifs = await notifDao.findAll(conn, options);

        assert.equal(notifs.length, 0);

        // 3.2. check that notif was successfully archived
        let archNotifs = await archNotifDao.findAll(conn, options);

        assert.equal(archNotifs.length, 1);
        assert.equal(archNotifs[0].ref, notif.ref);
        assert.equal(archNotifs[0].dest, notif.dest);
        assert.equal(archNotifs[0].destType, notif.destType);
        assert.equal(archNotifs[0].message, notif.message);        
        assert.deepEqual(archNotifs[0].model, notif.model || null);
    })

    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is ready \n' +
        '       | - notif.readyOn is not provided \n' +
        '       | - model not supplied \n' +
        '       | Expected :  \n' +
        '       | -> should fallback to default readyOn `new Date()`  \n' +
        '       | -> forward the message to a NotifProcessor  \n' +
        '       | -> archive the notif  \n' +
        '       | -> use message as provided  \n' 
        , async () => {

        // 1. prepare test-data 

        let notif = new Notif();
        notif.ref = chance.string({ length: 10 });
        notif.dest = chance.string({ length: 10 });
        notif.destType = "I"        
        notif.reason = chance.string({ length: 10 });        
        notif.message = chance.string({length: 100})

        let options: NotifPostOptions = {
            user: "__super_user__"
        }

        // 2. execute test
        await notifManager.post(conn, notif, options);

        // 3. assert result 
        //    give some time for the notif to be processed
        await wait(500);

        // 3.1. check that notif was deleted from `notif` table 
        let notifs = await notifDao.findAll(conn, options);

        assert.equal(notifs.length, 0);

        // 3.2. check that notif was successfully archived
        let archNotifs = await archNotifDao.findAll(conn, options);

        assert.equal(archNotifs.length, 1);
        assert.equal(archNotifs[0].ref, notif.ref);
        assert.equal(archNotifs[0].dest, notif.dest);
        assert.equal(archNotifs[0].destType, notif.destType);
        assert.equal(archNotifs[0].message, notif.message);        
        assert.deepEqual(archNotifs[0].model, notif.model || null);
    })

    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is not ready \n' +
        '       | - model not supplied \n' +
        '       | Expected :  \n' +
        '       | -> should not process notif \n'        
        , async () => {

        // 1. prepare test-data 

        let notif = new Notif();
        notif.ref = chance.string({ length: 10 });
        notif.dest = chance.string({ length: 10 });
        notif.destType = "I"
        notif.readyOn = new Date(new Date().getTime() + (60 * 60 * 1000)); /* ready in one hour */
        notif.reason = chance.string({ length: 10 });        
        notif.message = chance.string({length: 100})

        let options: NotifPostOptions = {
            user: "__super_user__"
        }

        // 2. execute test
        await notifManager.post(conn, notif, options);

        // 3. assert result 
        //    give some time for the notif to be processed
        await wait(500);

        // 3.1. check that notif exists in `notif` table 
        let notifs = await notifDao.findAll(conn, options);

        assert.equal(notifs.length, 1);
        assert.equal(notifs[0].ref, notif.ref);
        assert.equal(notifs[0].dest, notif.dest);
        assert.equal(notifs[0].destType, notif.destType);
        assert.equal(notifs[0].message, notif.message);        
        assert.deepEqual(notifs[0].model, notif.model || null);

        // 3.2. check that notif was not archived
        let archNotifs = await archNotifDao.findAll(conn, options);

        assert.equal(archNotifs.length, 0);        
        
    })

    
    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is not ready \n' +
        '       | - model not supplied \n' +
        '       | Expected :  \n' +
        '       | -> should not process notif \n'        
        , async () => {

        // 1. prepare test-data 

        let notif = new Notif();
        notif.ref = chance.string({ length: 10 });
        notif.dest = chance.string({ length: 10 });
        notif.destType = "I"
        notif.readyOn = new Date(new Date().getTime() + (60 * 60 * 1000)); /* ready in one hour */
        notif.reason = chance.string({ length: 10 });        
        notif.message = chance.string({length: 100})

        let options: NotifPostOptions = {
            user: "__super_user__"
        }

        // 2. execute test
        await notifManager.post(conn, notif, options);

        // 3. assert result 
        //    give some time for the notif to be processed
        await wait(500);

        // 3.1. check that notif exists in `notif` table 
        let notifs = await notifDao.findAll(conn, options);

        assert.equal(notifs.length, 1);
        assert.equal(notifs[0].ref, notif.ref);
        assert.equal(notifs[0].dest, notif.dest);
        assert.equal(notifs[0].destType, notif.destType);
        assert.equal(notifs[0].message, notif.message);        
        assert.deepEqual(notifs[0].model, notif.model || null);

        // 3.2. check that notif was not archived
        let archNotifs = await archNotifDao.findAll(conn, options);

        assert.equal(archNotifs.length, 0);        
        
    })



    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is ready \n' +
        '       | - model is supplied \n' +
        '       | - language code is `en` \n' +
        '       | Expected :  \n' +
        '       | -> forward the message to a NotifProcessor  \n' +
        '       | -> archive the notif  \n' +
        '       | -> use `en` template  \n'
        , async () => {

            // 1. prepare test-data 

            let notif = new Notif();
            notif.ref = chance.string({ length: 10 });
            notif.dest = chance.string({ length: 10 });
            notif.destType = "I"
            notif.languageCode = 'en'
            notif.readyOn = new Date();
            notif.reason = chance.string({ length: 10 });
            notif.subject = "WELCOME_SUBJECT"
            notif.message = 'WELCOME'

            let options: NotifPostOptions = {
                user: "__super_user__",                
            }
            let postOptions = {
                ...options,
                model: {
                    name: 'Omar'
                }
            }

            // 2. execute test
            await notifManager.post(conn, notif, postOptions);

            // 3. assert result 
            //    give some time for the notif to be processed
            await wait(500);

            // 3.1. check that notif was deleted from `notif` table 
            let notifs = await notifDao.findAll(conn, options);

            assert.equal(notifs.length, 0);

            // 3.2. check that notif was successfully archived
            let archNotifs = await archNotifDao.findAll(conn, options);

            assert.equal(archNotifs.length, 1);
            assert.equal(archNotifs[0].ref, notif.ref);
            assert.equal(archNotifs[0].dest, notif.dest);
            assert.equal(archNotifs[0].destType, notif.destType);
            assert.deepStrictEqual(archNotifs[0].message, 'Welcome ,Omar!');            
            assert.deepStrictEqual(archNotifs[0].subject, 'Welcome ,Omar!');            
            assert.deepEqual(archNotifs[0].model, notif.model || null);
        })

    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is ready \n' +
        '       | - model is supplied \n' +
        '       | - language code is `en` \n' +
        '       | Expected :  \n' +
        '       | -> forward the message to a NotifProcessor  \n' +
        '       | -> archive the notif  \n' +
        '       | -> use `fr` template  \n'
        , async () => {

            // 1. prepare test-data 

            let notif = new Notif();
            notif.ref = chance.string({ length: 10 });
            notif.dest = chance.string({ length: 10 });
            notif.destType = "I"
            notif.languageCode = 'fr'
            notif.readyOn = new Date();
            notif.reason = chance.string({ length: 10 });
            notif.subject = "WELCOME_SUBJECT"
            notif.message = 'WELCOME'

            let options: NotifPostOptions = {
                user: "__super_user__",
            }
            let postOptions = {
                ...options,
                model: {
                    name: 'Omar'
                }
            }

            // 1.1 create fr template

            let frTemplateBody = new I18nTempl()
            frTemplateBody.ownerId = templs.filter(templ => templ.code === "WELCOME")[0].id
            frTemplateBody.locale = 'fr'
            frTemplateBody.message = 'Bienvenu ,{{=model.name}}!'

            let frTemplateSubject = new I18nTempl()            
            frTemplateSubject.ownerId = templs.filter(templ => templ.code === "WELCOME_SUBJECT")[0].id
            frTemplateSubject.locale = 'fr'
            frTemplateSubject.message = 'Bienvenu ,{{=model.name}}!'

            await i18nTemplDao.createAll(conn, [frTemplateBody, frTemplateSubject], {user: "__super_user__"})
            

            // 2. execute test
            await notifManager.post(conn, notif, postOptions);

            // 3. assert result 
            //    give some time for the notif to be processed
            await wait(500);

            // 3.1. check that notif was deleted from `notif` table 
            let notifs = await notifDao.findAll(conn, options);

            assert.equal(notifs.length, 0);

            // 3.2. check that notif was successfully archived
            let archNotifs = await archNotifDao.findAll(conn, options);

            assert.equal(archNotifs.length, 1);
            assert.equal(archNotifs[0].ref, notif.ref);
            assert.equal(archNotifs[0].dest, notif.dest);
            assert.equal(archNotifs[0].destType, notif.destType);
            assert.deepStrictEqual(archNotifs[0].message, 'Bienvenu ,Omar!');
            assert.deepStrictEqual(archNotifs[0].subject, 'Bienvenu ,Omar!');
            assert.deepEqual(archNotifs[0].model, notif.model || null);
        })


    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is ready \n' +
        '       | - model is supplied \n' +
        '       | - model.message is template \n' +
        '       | - model.subject is not supplied \n' +
        '       | - language code is `en` \n' +
        '       | Expected :  \n' +
        '       | -> forward the message to a NotifProcessor  \n' +
        '       | -> archive the notif  \n' +
        '       | -> use `en` template  \n' +
        '       | -> use empty subject  \n'
        , async () => {

            // 1. prepare test-data 

            let notif = new Notif();
            notif.ref = chance.string({ length: 10 });
            notif.dest = chance.string({ length: 10 });
            notif.destType = "I"
            notif.languageCode = 'en'
            notif.readyOn = new Date();
            notif.reason = chance.string({ length: 10 });            
            notif.message = 'WELCOME'

            let options: NotifPostOptions = {
                user: "__super_user__",
            }
            let postOptions = {
                ...options,
                model: {
                    name: 'Omar'
                }
            }

            // 2. execute test
            await notifManager.post(conn, notif, postOptions);

            // 3. assert result 
            //    give some time for the notif to be processed
            await wait(500);

            // 3.1. check that notif was deleted from `notif` table 
            let notifs = await notifDao.findAll(conn, options);

            assert.equal(notifs.length, 0);

            // 3.2. check that notif was successfully archived
            let archNotifs = await archNotifDao.findAll(conn, options);

            assert.equal(archNotifs.length, 1);
            assert.equal(archNotifs[0].ref, notif.ref);
            assert.equal(archNotifs[0].dest, notif.dest);
            assert.equal(archNotifs[0].destType, notif.destType);
            assert.deepStrictEqual(archNotifs[0].message, 'Welcome ,Omar!');
            assert.deepStrictEqual(archNotifs[0].subject, '');
            assert.deepEqual(archNotifs[0].model, notif.model || null);
        })


    it('| #process  \n' +
        '       | Conditions: \n' +
        '       | - notif is ready \n' +
        '       | - model is supplied \n' +
        '       | - model.message is template \n' +
        '       | - model.subject is not supplied \n' +
        '       | - language code is `en` \n' + 
        '       | - sourceMarkup is md \n' + 
        '       | - targetMarkup is html \n' + 
        '       | Expected :  \n' +
        '       | -> forward the message to a NotifProcessor  \n' +
        '       | -> archive the notif  \n' +
        '       | -> use `en` template  \n' +
        '       | -> use empty subject  \n'
        , async () => {

            // 1. prepare test-data 

            let notif = new Notif();
            notif.ref = chance.string({ length: 10 });
            notif.dest = chance.string({ length: 10 });
            notif.destType = "I"
            notif.languageCode = 'en'
            notif.readyOn = new Date();
            notif.reason = chance.string({ length: 10 });
            notif.message = 'WELCOME'            

            let options: NotifPostOptions = {
                user: "__super_user__",
            }
            let postOptions = {
                ...options,
                model: {
                    name: 'Omar',
                    templOptions: {
                        sourceMarkup: 'MD',
                        targetMarkup: 'HTML',
                    }
                }
            }

            // 2. execute test
            await notifManager.post(conn, notif, postOptions);

            // 3. assert result 
            //    give some time for the notif to be processed
            await wait(500);

            // 3.1. check that notif was deleted from `notif` table 
            let notifs = await notifDao.findAll(conn, options);

            assert.equal(notifs.length, 0);

            // 3.2. check that notif was successfully archived
            let archNotifs = await archNotifDao.findAll(conn, options);

            assert.equal(archNotifs.length, 1);
            assert.equal(archNotifs[0].ref, notif.ref);
            assert.equal(archNotifs[0].dest, notif.dest);
            assert.equal(archNotifs[0].destType, notif.destType);
            assert.deepStrictEqual(archNotifs[0].message, '<p>Welcome ,Omar!</p>\n');
            assert.deepStrictEqual(archNotifs[0].subject, '');
            assert.deepEqual(archNotifs[0].model, notif.model || null);
        })
})