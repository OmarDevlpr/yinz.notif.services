// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import TemplEngine from "main/ts/TemplEngine";



const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();

const templEngine: TemplEngine = container.getBean<TemplEngine>('templEngine');


describe('| notif.services.TemplEngine', function () {

    it('| # extrapolate ( ) -->  \n' +
        '      | Conditions: \n' +
        '      | - templ is valid template \n' +
        '      | - model is valid object \n' +
        '      | Expected: \n' +
        '      | - should extrapolate template succesfully \n' +
        '', async () => {

            // 1. prepate test data
            let model = {
                name: chance.string()
            }
            let templPrefix = chance.string({ length: 100 }) + " ";
            let templNamePlaceHolder = "{{=model.name}}" + " ";
            let templSuffix = chance.string({ length: 100 }) + "";

            let templ = templPrefix + templNamePlaceHolder + templSuffix;

            // 2. execute test
            let result = await templEngine.extrapolate(templ, { model });

            // 3. assert result
            let expected = templPrefix + model.name + " " + templSuffix;
            assert.deepStrictEqual(result, expected)
        });

    it.skip('| # extrapolate ( ) -->  \n' +
        '      | Conditions: \n' +
        '      | - templ is valid template \n' +
        '      | - model is empty \n' +
        '      | Expected: \n' +
        '      | - should extrapolate template succesfully \n' +
        '      | - should replace placeholder with empty string \n' +
        '', async () => {

            // 1. prepate test data
            let model = {
                foo: chance.string()
            }
            let templPrefix = chance.string({ length: 100 }) + " ";
            let templNamePlaceHolder = "{{=model.name}}" + " ";
            let templSuffix = chance.string({ length: 100 }) + "";

            let templ = templPrefix + templNamePlaceHolder + templSuffix;

            // 2. execute test
            let result = await templEngine.extrapolate(templ, { model });

            // 3. assert result
            let expected = templPrefix + "" + " " + templSuffix;
            assert.deepStrictEqual(result, expected)
        });

    it('| # extrapolate ( ) -->  \n' +
        '      | Conditions: \n' +
        '      | - templ is empty string \n' +
        '      | - model is empty \n' +
        '      | Expected: \n' +
        '      | - should throw error template empty \n' +
        '', async () => {

            // 1. prepate test data
            let model = {
                foo: chance.string()
            }

            let templ = ""

            try {
                // 2. execute test
                await templEngine.extrapolate(templ, { model });

                assert.ok(false, 'bad flow... an exception is supposed to be fired.')

            } catch (e) {
                // 3. assert result                    
                assert.deepStrictEqual(e.reason, "ERR_NOTIF_SRV__TEMPL_ENG__MISS_TEMPL")
                assert.deepStrictEqual(e.extra, { templ: "", subject: "" })
            }

        });


    it('| # convert ( ) -->  \n' +
        '      | Conditions: \n' +
        '      | - markup is valid string \n' +
        '      | - options is valid \n' +
        '      | Expected: \n' +
        '      | - should convert MD to HTML successfuly \n' +
        '', async () => {

            // 1. prepate test data        

            let markup = "Hello"
            let options = {
                sourceMarkup: "md",
                targetMarkup: "html"
            }

            // 2. execute test
            let result = await templEngine.convert(markup, options);

            // 3. assert result
            let expected = "<p>Hello</p>\n"
            assert.deepStrictEqual(result, expected);

        });

});
