// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import { Logger } from "@yinz/commons";
import Notif from "@yinz/notif.data/models/Notif";
import EmailNotifProcessor from "../../main/ts/EmailNotifProcessor";
import {SMTPServer} from "smtp-server";
import { promisify } from "util";

const wait = promisify(setTimeout);

const chance = new Chance();
const smtpPort = 9025;

const container = new Container({
    paramsFile: 'params.yml',
    params: {
        email: {
            host: 'localhost',
            port: smtpPort,
            name: 'localhost',
            ignoreTLS: true,
            from: 'omar.mahdden@localhost',
            maxConnections: 5,
            maxMessages: 100,
            debug: true
        }
    },
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',           
        process.cwd() + '/dist/main/ts'
    ],
});

// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');
const logger     = container.getBean<Logger>('logger');
const emailNotifProcessor = container.getBean<EmailNotifProcessor>('emailNotifProcessor')

let conn: YinzConnection;
let trans: YinzTransaction;

let server;

const createServer = () => {
    
    server = new SMTPServer({
        closeTimeout: 1500,
        disabledCommands: ['AUTH', 'STARTTLS'],
        onAuth(auth, session, callback) { callback(null, { user: 123 }); },
        onConnect(session, cb ) {
            let domain = session.remoteAddress.split('@')[1] || 'localhost';
            if (domain === 'localhost') {
                return cb();
            }
            else {
                return cb(new Error("No connections from localhost allowed"));
            }
        },
        onData(stream, session, callback) {
            stream.pipe(process.stdout);
            stream.on("end", callback);
        },
        onClose(session) {
            logger.info(session.clientHostname, 'is closing...')
        }
        
    });    

    server.listen(smtpPort);
}

describe('| notif.services.EmailNotifProcessor',  () => {
    // this.timeout(60000);

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            createServer();
            await wait(500)
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            await server.close();
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });

    it(' process () => should send email to its destination', async () => {

        // 1. prepare test-data
        let notif: Notif = new Notif;
        notif.reason = chance.string({length: 10});
        notif.dest   = 'omar.mhadden@localhost';
        notif.destType = 'E';
        notif.subject = 'Hello, from Omar!';    
            

        // 2. execute test
        let status = await emailNotifProcessor.process(notif, 'Hello, from Omar body.');

        // 3. check outcome
        assert.ok(status.messageId);
        assert.deepEqual(status.accepted, [notif.dest]);
        assert.deepEqual(status.rejected, []);

    })


    // it.only(' #notifManager.post () => should send email to its destination then archive it', async () => {

    //     throw Error("USE CONNECTION INSTEAD OF TRANSACTION...")

    //     // 1. prepare test-data
    //     let notif: Notif = new Notif;
    //     notif.reason = chance.string({ length: 10 });
    //     notif.dest = 'omar.mhadden@localhost';
    //     notif.destType = 'E';
    //     notif.subject = 'Hello, from Omar!';

    //     // 2. execute test

    //     try {
    //         // 2.1 start notif manager to process notif
    //         await notifManager.startup(trans, {user: "__super_user__"});

    //         // 2.2 post a notification
    //         await notifManager.post(trans, notif)        

    //         // 2.3 give notifManager some time to process notif
    //         await wait(500);

    //         // 3. check outcome
            
    //         // 3.1 check that notif was deleted
    //         let notifsResult = await notifDao.findAll(trans, {user: "__super_user__"})
    //         assert.deepStrictEqual(notifsResult.length, 0);

    //         // 3.2 check that notif was archived 
    //         let archNotifsResult = await archNotifDao.findAll(trans, { user: "__super_user__" })
    //         assert.equal(archNotifsResult.length, 1);
    //         assert.equal(archNotifsResult[0].id, notif.id);
    //         assert.equal(archNotifsResult[0].dest, notif.dest);
    //         assert.equal(archNotifsResult[0].destType, notif.destType);
    //         assert.equal(archNotifsResult[0].message, notif.message);            
    //         assert.deepEqual(archNotifsResult[0].model, notif.model || null);


    //     } catch ( e ) {

    //     } finally {
    //         await notifManager.shutdown();
    //     }

    // })

 });