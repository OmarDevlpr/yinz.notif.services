// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import Transporter from "main/ts/Transporter";


const container = new Container({
    params: {
        transporter: {
            mode: "same-host"
        }
    },
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const transporter = container.getBean<Transporter>("transporter");


describe('| notif.services <Transporter__same-host>', function () {


    after(() => {
        transporter.release();
    });


    it.skip('\n' +
        '       | Skip: \n' +
        '       | IPC is not supported on windows \n' +
        '       | Conditions: \n' +
        '       | - data is string \n' +
        '       | Expected :  \n' +
        '       | -> should send a string  \n' +
        '       | -> should receive a string  \n' +
        '', async () => {

            // 1. prepare test-data 

            let data = chance.string({ length: 2000 });
            let options = {};



            setTimeout(() => {
                transporter.send(data, options)
            }, 0);

            let message = await new Promise((resolve, reject) => {
                transporter.on("message", (msg) => {
                    resolve(msg);
                })
            })

            assert.strictEqual(data, message)


        });


    it.skip('\n' +
        '       | Skip: \n' +
        '       | IPC is not supported on windows \n' +
        '       | Conditions: \n' +
        '       | - data is an object \n' +
        '       | Expected :  \n' +
        '       | -> should send an object  \n' +
        '       | -> should receive an object  \n' +
        '', async () => {

            // 1. prepare test-data 

            let data = {
                field1: chance.string({ length: 3000 }),
                field2: chance.string({ length: 3000 }),
                field3: chance.string({ length: 3000 }),
                field4: chance.string({ length: 3000 }),
                field5: chance.string({ length: 3000 }),
                field6: chance.string({ length: 3000 })
            }
            let options = {};



            setTimeout(() => {
                transporter.send(data, options)
            }, 0);

            let message = await new Promise((resolve, reject) => {
                transporter.on("message", (msg) => {
                    resolve(msg);
                })
            })

            assert.deepStrictEqual(data, message)


        });



});
