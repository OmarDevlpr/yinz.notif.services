import { Logger, YinzOptions, Asserts, Exception } from "@yinz/commons";
import * as mustache from "mustache";
import * as dot from "dot";
import * as _marked from "marked";
import * as util from 'util';
import * as moment from 'moment';
import * as numeral from 'numeral';
import * as _ from "lodash";

let marked = util.promisify(_marked);


// configure dot template engine
const DFLT_TEMPL_CONFIG = {
    evaluate: /\{\{([\s\S]+?)\}\}/g,              // {{ }}  for evaluation
    interpolate: /\{\{=([\s\S]+?)\}\}/g,              // {{= }}  for interpolation
    encode: /\{\{!([\s\S]+?)\}\}/g,              // {{! }}  for interpolation with encoding (HTML escaping)
    use: /\{\{#([\s\S]+?)\}\}/g,              // {{# }}  for compile-time evaluation/includes and partials
    define: /\{\{##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\}\}/g,  // {{## #}}  for compile-time defines
    conditional: /\{\{\?(\?)?\s*([\s\S]*?)\s*\}\}/g,        // {{? }}  for conditionals
    iterate: /\{\{~\s*(?:\}\}|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\}\})/g,
    // {{~ }}  for array iteration
    varname: 'model',
    strip: false,    // To control whitespace use 'strip' option, true - to strip,
    // false - to preserve.
    append: false,    // a performance optimization setting. It allows to tweak 
    // performance, depending on the javascript engine used and
    // size of the template, it may produce better results with
    // append set to false.
    selfcontained: false,   // If 'selfcontained' is true, doT will produce functions that
    // have no dependencies on doT. In general, generated functions
    // have no dependencies on doT, with the exception for 
    // encodeHTML and it is only added if encoding is used.
    // If 'selfcontained' is true and template needs encoding,
    // encodeHTML function will be included in the generated
    // template function. 
};



export interface TemplEngineOptions {
    logger: Logger;
    templHome?: string;
}

export default class TemplEngine {

    private logger: Logger;
    private templHome: string;

    constructor(options: TemplEngineOptions) {
        this.logger = options.logger;
        this.templHome = options.templHome || "src/main/templ";

        this.templHome;
    }


    public async convert(markup: string, options: { sourceMarkup: string, targetMarkup: string }): Promise<string> {

        if (['md', 'markdown', 'MD'].includes(options.sourceMarkup && options.sourceMarkup.toLocaleLowerCase())
            && ['html', 'htm', 'HTML'].includes(options.targetMarkup && options.targetMarkup.toLocaleLowerCase())) {
            return this.convertMarkupToHtml(markup);
        } else if (options.sourceMarkup || options.targetMarkup) {
            throw new Exception('ERR_NOTIF_SRV__TEMPL_ENG__UNEXP_CONVERSION', {
                message: `Sorry, I'm unable to find my way as I' not expecting to receive sourceMarkup [${options.sourceMarkup}] and targetMarkup [${options.targetMarkup}]!`
            });
        }

        return markup;
    }

    private async convertMarkupToHtml(markup: string): Promise<string> {
        return await marked(markup, { sanitize: true });
    }

    public async extrapolate(templ: string, model: any, options?: any): Promise<string> {

        options = options || {};

        this.logger.trace(`--(${templ}, ${model}, ${options}){`);

        // 1. assert non empty template
        Asserts.isNonEmptyString(templ, "ERR_NOTIF_SRV__TEMPL_ENG__MISS_TEMPL", { templ });

        // for backwards compatibilty, by defaut we will use dot
        if (options.engine === 'mustache') {
            return this.extrapolateUsingMustache(templ, model, options);
        }

        return this.extrapolateUsingDot(templ, model, options);
    }

    private extrapolateUsingMustache(templ: string, model: any, options?: YinzOptions): string {
        return mustache.render(templ, model);
    }

    private extrapolateUsingDot(templ: string, model: any, options?: YinzOptions): string {
        
        // @ts-ignore 
        dot.templateSettings = DFLT_TEMPL_CONFIG;

        let compiled = dot.compile(templ);
        if (this.logger) {
            this.logger.debug('    compiled template function [%s]', compiled.toString());
        }

        let modelClone = {...model.model};        
     
        return compiled(_.extend({}, modelClone, { libs: { numeral: numeral, moment: moment, _: _ } }));

    }

}