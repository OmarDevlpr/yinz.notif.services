import  {Exception, YinzOptions, Logger, Asserts } from "@yinz/commons";
import Transporter  from "./Transporter";
import { DataSource, TypeormDao } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import INotifProcessor from "./INotifProcessor";
import Notif from "@yinz/notif.data/models/Notif";
import Templ from "@yinz/notif.data/models/Templ";
import ArchNotif from "@yinz/notif.data/models/ArchNotif";

import {promisify} from "util";
import TemplEngine from "./TemplEngine";

const wait = promisify(setTimeout);


const MAX_NUMB_OF_ERRS = 10;
const ERR_WATCH_PERIOD = 10;

export interface NotifPostOptions extends YinzOptions {
    model?: any
}

export interface ListenerMessage {
    type: "shutdown" | "notif",
    procDate: Date,
    notifs: Notif[] 
}

export interface PromiseContext {
    resolve: Function;
    reject: Function;
    numOfErrors?: number;
    lastErrTimestamp?: Date;
} 

export interface NotifProcessor {
    prepareData(...args);
    process(notif, message, subject);

}

export interface NotifManagerOptions {
    logger: Logger;
    transporter: Transporter;
    templEngine: TemplEngine;
    dataSource: DataSource;
    notifDao: TypeormDao<Notif>;
    archNotifDao: TypeormDao<Notif>;
    templDao: TypeormDao<Templ>;
}   

export interface NotifManagerStartupOptions extends YinzOptions {
    procDate?: Date;
    pollingTimer?: number    
    proccessors?: INotifProcessor[];
}

export type DEST_TYPE = "E" | "S" | "I";



export default class NotifManager {

    private dataSource: DataSource;
    private transporter: Transporter;
    private logger: Logger;
    private templEngine: TemplEngine;

    private procDate: Date;
    private pollingTimer: number;
    private notifProccessors: Map<DEST_TYPE, INotifProcessor>;

    private notifDao: TypeormDao<Notif>;
    private archNotifDao: TypeormDao<Notif>;
    private templDao: TypeormDao<Templ>;

    private notifsPollerStarted: boolean;
    private shutdownRequested: boolean;

    constructor(options: NotifManagerOptions) {

        this.dataSource = options.dataSource;
        this.transporter = options.transporter;
        this.logger = options.logger;
        this.templEngine = options.templEngine;
        
        this.notifDao = options.notifDao;
        this.archNotifDao = options.archNotifDao;
        this.templDao = options.templDao;

        this.notifsPollerStarted = false;
        this.shutdownRequested = false;

        this.notifProccessors = new Map<DEST_TYPE, INotifProcessor>();
    }

    public async post(handler: YinzConnection | YinzTransaction, notif: Notif, options?: NotifPostOptions ): Promise<void> {

        options = options || {};

        let trans: YinzTransaction | null = null;

        try {

            // 1. validate notif fields
            Asserts.isNonEmptyString(notif.dest, 'ERR_NOTIF_MNG__POST__INV_DEST', {
                message: `The supplied notif has an invalid destination ${notif.dest}! It should be a none-empty string.`,
                dest: notif.dest,
                notif: notif
            })

            // 2. open new transaction if handler is a connection            
            if ( handler.type === "conn" )
                trans = await this.dataSource.startTrans(handler)
            
            // 3. create notif
            notif.model = options.model || notif.model;
            notif.readyOn = notif.readyOn || new Date();
            notif.procDate = notif.procDate || new Date();
            notif.procStatus = notif.readyOn.getTime() <= notif.procDate.getTime() ? 'I' /*In progress*/ : 'W' /*Waiting*/;

            notif = await this.notifDao.create(trans || handler, notif, options)
            
            // 4. commit transaction if we started a new one
            if ( trans )     
                await this.dataSource.commitTrans(trans);

            // 5. process notification incase it is ready
            this.logger.debug('Notif [%s] via [%s] has been posted successfully. [%j]', notif.ref, notif.destType, notif);
            if (notif.readyOn.getTime() <= notif.procDate.getTime() )
                await this.transporter.send({ type: "notif", procDate: notif.procDate, notifs: [notif]}, options);            

        } catch ( e ) {

            // 6. rollback transaction if we started a new one
            if ( trans )
                await this.dataSource.rollbackTrans(trans)

            // rethrow error            
            throw e;
        }
        
    }

    public async startup(handler: YinzConnection | YinzTransaction, options?: NotifManagerStartupOptions): Promise<void> {

        options = options || {};

        this.logger = options && options.logger || this.logger;
        this.procDate = options && options.procDate || new Date();
        this.pollingTimer = options && options.pollingTimer || 10000; // ms                                  

        // start listening for notif from transporter
        let listenerNotifs = new Promise( (resolve, reject) => {
            this.notifListenerLoop(handler, {resolve, reject})
        });            

        // start polling notifs from db
        let pollingNotifs = new Promise( (resolve, reject) => {
            this.notifPollerLoop(handler, {resolve, reject}, this.procDate, this.pollingTimer);
        })

        
        

        await Promise.all([listenerNotifs, pollingNotifs]);


    }

    private async tooManyErrors(context: PromiseContext, options?: YinzOptions) {

        let result = false;

        let now = new Date();
        context.numOfErrors = context.numOfErrors || 0;
        context.lastErrTimestamp = context.lastErrTimestamp || now;

        this.logger.debug('nbrOfErrs: %d, lastErrTimestamp: %d, now: %d', context.numOfErrors, context.lastErrTimestamp.getTime(), now.getTime());

        if (context.lastErrTimestamp.getTime() + ERR_WATCH_PERIOD > now.getTime()) {
            // in case more than 10ms separate this error from then latest error, reset the counter
            context.numOfErrors = 0;
        }
        else {
            context.numOfErrors++;
            if (context.numOfErrors >= MAX_NUMB_OF_ERRS) {
                result = true;
            }
        }

        return result;
    }

    private async extrapolate(templ: string, model: any, options?: YinzOptions): Promise<string> {
     
        this.logger.debug(`extrapolating templ [%s] with model [%j]`, templ, model)
        return await this.templEngine.extrapolate(templ, {model});        
    }    

    private async lookupTempl(handler: YinzConnection | YinzTransaction, notifDestType: DEST_TYPE, notifTemplCode: string, notifLangCode: string, options?: YinzOptions): Promise<Templ> {

        this.logger.debug(`Will lookup the template using notifDestType [${notifDestType}], notifTemplCode [${notifTemplCode}], and notifLangCode [${notifLangCode}]...`);
        let templCode: string;
        let templ: Templ
         
        // fetch template
        templCode = notifDestType + '/' + notifTemplCode;
        templ = (await this.templDao.findByFilter(handler, {code: templCode}, {...options, locale: notifLangCode}))[0];        
        if (templ !== null && templ !== undefined)
            return templ;            

        // fetch template without locale
        templCode = notifDestType + '/' + notifTemplCode;
        templ = (await this.templDao.findByFilter(handler, { code: templCode }, { ...options }))[0];
        if (templ !== null && templ !== undefined)
            return templ;            
        

        // fetch template using default notifTemplCode
        templCode = notifTemplCode;
        templ = (await this.templDao.findByFilter(handler, { code: templCode }, { ...options, locale: notifLangCode }))[0];
        if (templ !== null && templ !== undefined)
            return templ;            
        
        // fetch template using default notifTemplCode without locale
        templCode = notifTemplCode;
        templ = (await this.templDao.findByFilter(handler, { code: templCode }, { ...options }))[0];
        if (templ !== null && templ !== undefined)
            return templ;            
        
        throw new Exception('ERR_NOTIF_SRV__LOOKUP_TEMPL__TEMPL_NOT_FOUND', {
            message: `Missing template [${notifTemplCode}] for language [${notifLangCode}] and destination [${notifDestType}]!`
        });
    }

    /**
     * This method aims to prepare the notification subject or title by inserting the model data into the template     
     */
    private async prepareNotifSubject(trans: YinzTransaction, notif: Notif, notifMessageTempl?: Templ | null, options?: YinzOptions): Promise<string> {

      
        // 1. incase notif has empty model, return the subject as is 
        if ( !notif.model ) {
            
            // in case the title of the notif's message is defined, it take precedence on notif's subject
            let subject = (notifMessageTempl && notifMessageTempl.title) || notif.subject;

            return subject;
        }
        

        // 2. notif's template title takes precedence over notif subject 
        if ( notifMessageTempl && notifMessageTempl.title ) 
            return await this.extrapolate(notifMessageTempl.title, notif.model, options);
        

        // 3. if notif subject is falsy return empty string
        if ( !notif.subject ) 
            return '';        
        
        // 4. at this point we're that we're dealing with `notif.subject`        
        // 4.1. lookup the template first         
        let templ: Templ = await this.lookupTempl(trans,
            notif.destType,
            notif.subject,
            notif.languageCode,
            { ...options, locale: notif.languageCode }
        );
        

        this.logger.debug(`about to extrapolate... [%j]`, templ)
        return await this.extrapolate(templ.message, notif.model, options);                        
    }
    
    private async convert(markup: string, model: any, options?: YinzOptions): Promise<string> {

        let opts = {
            ...options,
            sourceMarkup: model && model.templOptions && model.templOptions.sourceMarkup,
            targetMarkup: model && model.templOptions && model.templOptions.targetMarkup,
        }

        return await this.templEngine.convert(markup, opts);
    }

    /**
     * this method aims to prepare the notif body using the template and `notif.model` data
     */
    private async prepareNotifMessage(handler: YinzTransaction | YinzConnection, notif: Notif, notifMessageTempl?: Templ | null, options?: YinzOptions): Promise<string> {

        // incase notif has empty model, return the subject as is 
        if (!notif.model)                        
            return notif.message;
        

        // prepare notif message

        let templ: Templ = await this.lookupTempl(handler,
            notif.destType,
            notifMessageTempl && notifMessageTempl.code || notif.message,
            notif.languageCode,
            { ...options, locale: notif.languageCode }
        );

        let result =  await this.extrapolate(templ.message, notif.model, options);          

        return await this.convert(result, notif.model, options);        
    }

    private async processNotif(handler: YinzConnection | YinzTransaction, procDate: Date, notif: Notif, processor: INotifProcessor, options?: YinzOptions): Promise<Notif> {

        
        options = {...options, user: "__super_user__"}

        let subject: string;
        let message: string;
        let notifMessageTempl: Templ | null = null;
        let archived = false;
        let deleted = false;
        let trans: YinzTransaction | null = null;
        
        // update notif.model
        // with the usage of get and set in typescript, we can get the private model and JSON stringify it.
        // however when we send the message via the transporter, for some reason the `_model` is sent instead of `model`
        // here we switch them

        // @ts-ignore
        notif.model = JSON.parse(notif._model)
        // @ts-ignore
        delete notif._model;

        try {
            // 1. open a new transaction (for each notif)
             trans = await this.dataSource.startTrans(handler, options);

            // 2. lookup message template if any                  
            if ( notif.model)      
                notifMessageTempl = await this.lookupTempl(trans, notif.destType, notif.message, notif.languageCode, {...options, locale: notif.languageCode});            

            // 3. prepare notif's subject          
            this.logger.debug(`about to prepare Notif Subject...`)   
            this.logger.debug(`${JSON.stringify(notif)}`)   
            subject = await this.prepareNotifSubject(trans, notif, notifMessageTempl, options);

            // 4. prepare notif's message (in the case of Object for Instant Messaging, we will JSON stringify it, and let the reciever parse it)
            this.logger.debug(`about to prepare Notif Message...`)
            this.logger.debug(`${JSON.stringify(notif)}`)   
            message = await this.prepareNotifMessage(trans, notif, notifMessageTempl, options);

            // 5. process the notif
            await processor.process(notif, message, subject);

            // 6. archive the notif
            // * subject and message should be archived as they were sent
            // * update notif status to 'S' (sent) incase the processor didn't change it            
            let archNotif: ArchNotif = new ArchNotif();
            archNotif.id = notif.id
            archNotif.createdBy = notif.createdBy;
            archNotif.createdOn = notif.createdOn;
            archNotif.dest = notif.dest;
            archNotif.destType = notif.destType;
            archNotif.languageCode = notif.languageCode;
            archNotif.lastUpdateBy = notif.lastUpdateBy;
            archNotif.lastUpdateOn = notif.lastUpdateOn;
            archNotif.message = message;
            archNotif.model = notif.model;
            archNotif.procDate = notif.procDate;
            archNotif.procMessage = notif.procMessage;
            archNotif.procStatus = (notif.procStatus === "I" ? "S" : notif.procStatus);
            archNotif.readyOn = notif.readyOn;
            archNotif.reason = notif.reason;
            archNotif.ref = notif.ref;
            archNotif.subject = subject            
            archNotif.archOn = new Date();            

            // archNotif = {
            //     ...notif,                                           
            //     archOn: new Date(),
            //     subject: subject,
            //     message: message,
            //     procStatus: (notif.procStatus === "I" ? "S" : notif.procStatus),                                        
            // };
            
            // archNotif.model = notif.model;

            await this.archNotifDao.create(trans, archNotif, options);
            archived = true;

            // 7. delete notif
            await this.notifDao.deleteById(trans, notif.id, options)
            deleted = true;
            
        } catch ( e ) {

            // 8. update notif's status in case of error

            if ( ! deleted  && trans) {

                if ( !archived )
                    this.logger.error('notif [%d] was not archived!', notif.id);
                    this.logger.error(e);

                this.notifDao.updateById(trans as YinzTransaction, {
                    procStatus: "E",
                    procMessage: e.stack || e.message,
                    procDate: new Date()
                }, notif.id, options);
            }                        

        } finally {

            // 9 . commit in all cases
            await this.dataSource.commitTrans(trans as YinzTransaction, options);
        }

        // 10. return notif
        return notif;
    }

    // proccesses notifications
    private async processNotifs(handler: YinzConnection | YinzTransaction, procDate: Date, notifs: Notif[], options?: YinzOptions) {

        this.logger.info("received [%d] notifs for processing", notifs.length)
        this.logger.info("received [%j] ", notifs)
        // asynchronously loop trough notifications in the array

        for ( let notif of notifs) {

            // 1. make sure that we have a proccessor for the notif dest
            
            let processor = this.notifProccessors.get(notif.destType);

            if ( !processor ) {
                // throw error processor not found
                this.logger.error(' the processor was not found for some reason init')
            } else {

                // 2. process notif
                options = {
                    ...options,
                    user: "__super_user__"
                }

                // do not await on this, as we want them to run asynchronously
                // or not, (TODO check these in depth later)
                await this.processNotif(handler, procDate, notif, processor, options)
            }
        }

    }

    // this method aims to listen for notifications it receives in the transporter. Message could either arrive
    // directly from an external service, or the notif poller will fetch the ready notifs from the database and
    // forward them to the transporter.
    private async notifListenerLoop(handler: YinzConnection | YinzTransaction, context: PromiseContext): Promise<void> {

        try {

            // event handler, processes notifs or shutsdown the listener
            let listener: Function = (message: ListenerMessage ): any => {

                let type = message.type;
                let procDate = message.procDate;
                let notifs = message.notifs as Notif[];

                if ( type === "notif") {

                    let options: YinzOptions = {
                        user: "__super_user__"
                    }                    
                    return this.processNotifs(handler, procDate, notifs, options)
                } else if ( type === "shutdown") {

                    // stop listening to new messages
                    this.transporter.on('removeListener', (eventName, listener) => {

                        this.logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
                        this.logger.info('~~             S T O P   A C C E P T I N G   N O T I F S   F O R   P R O C E S S I N G            ~~');
                        this.logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
                        return;
                    });
                    this.transporter.removeListener('message', listener as any);
                } else {

                    this.logger.error('Unexpected type of messages [%d]!', message.type);
                    this.logger.error('%j', message);                        
                }                
            }

            this.transporter.on("message", listener as any);


        } catch (e) {

            // log the error
            this.logger.warn('%s', e.stack || e.message);

            // make sure we are not looping withing a continues error loop
            if (this.tooManyErrors(context)) {
                this.logger.error(
                    'Will shutdown the manager as there are [%d] errors in less than [%d]ms.',
                    context.numOfErrors, MAX_NUMB_OF_ERRS * ERR_WATCH_PERIOD
                );
                this.shutdown();
            }

        } finally {

            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            this.logger.info('!!                    S H U T T I N G   D O W N   N O T I F I S   P R O C E S S O R               !!');
            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');

            context.resolve();

            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            this.logger.info('!!                        N O T I F I S   P R O C E S S O R   I S   O V E R                       !!');
            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        }

    }

    // pull notifs from database and forward them to the notif processor loop using `transporter`
    private async notifPollerLoop(handler: YinzConnection | YinzTransaction, context: PromiseContext, procDate: Date, pollingTimer: number, options?: YinzOptions): Promise<void> {                                        
                
        options = {};

        if (! this.notifsPollerStarted ) {
            this.notifsPollerStarted = true;
            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            this.logger.info('!!                             S T A R T   N O T I F S   P O L L E R                              !!');
            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        }

        // stop in case shutdown order has been received
        if ( this.shutdownRequested ) {
            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            this.logger.info('!!                     S H U T T I N G   D O W N   N O T I F S   P O L L E R                      !!');
            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');

            context.resolve();

            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            this.logger.info('!!                          N O T I F I S   P O L L E R   I S   O V E R                           !!');
            this.logger.info('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');

            return;
        }

        
        let trans: YinzTransaction | null = null;

        try { 

            this.logger.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            this.logger.debug('~~                     S T A R T   A   N E W   P O L L I N G   S E S S I O N                      ~~');
            this.logger.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

            // 1. start new transaction
            trans = await this.dataSource.startTrans(handler, options)

            // 2. retrieve ready notifs
            let notifs = await this.notifDao.findByFilter(trans, {
                procStatus: 'W',
                // @ts-ignore TODO REMOVE THIS CHECK 
                readyOn: { op: "<", val: procDate.toISOString()},
            }, { ...options, user: "__super_user__" });

            

            // TODO:
            //   should update notification processing status to avoid duplicate
            //   processing in case of multiple agents.

            if ( notifs.length > 0) {

                this.logger.debug('Will send [%d] notif(s) for processing...', notifs.length);


                notifs.forEach( notif => this.logger.trace(`notif ----> ${notif.model}`))

                // mark notifs as in progress
                notifs = notifs.map( notif => ({...notif, procStatus: "I"}));
                let notifsIds = notifs.map(notif => notif.id);
                
                await this.notifDao.updateByFilter(
                    trans,
                    { procStatus: "I" },
                    { id: { op: "IN", val: notifsIds } },
                    {...options, user: "__super_user__"});


                let transporterMessage: ListenerMessage = {
                    type: "notif",
                    procDate: procDate,
                    notifs: notifs
                };            

                await this.transporter.send(transporterMessage, options);

            }   

            // commit trans
            await this.dataSource.commitTrans(trans, options);

            procDate = new Date(procDate.getTime() + pollingTimer);

            // cooldown for a moment
            await wait(pollingTimer);

            await this.notifPollerLoop(handler, context, procDate, pollingTimer, options);

            
        } catch (e) {

            // close the current transaction
            if (trans !== null) {
                await this.dataSource.commitTrans(trans, options);
            }

            // // make sure we are not looping withing a continues error loop
            if (this.tooManyErrors(context, options)) {
                this.logger.error(
                    'Will shutdown the manager as there are [%d] errors in less than [%d]ms.',
                    context.numOfErrors, MAX_NUMB_OF_ERRS * ERR_WATCH_PERIOD
                );
                this.shutdown(options);

                process.nextTick(this.notifPollerLoop, handler, context, procDate, pollingTimer , options);
            }
                        
        }

    }

    public async shutdown(options?: YinzOptions): Promise<void> {

        this.shutdownRequested = true;

        // stop waiting for messages
        this.transporter.send({type: "shutdown"})

    }

    public async release(): Promise<void> {
        await this.transporter.release();
    }

    // sbuscribes a notif proccessor
    public subscribe(destType: DEST_TYPE, proccessor: INotifProcessor) {

        this.notifProccessors.set(destType, proccessor);

        this.logger.info('subscribed processor [%s] for dest type [%s]', proccessor.constructor.name, destType)

    }

}