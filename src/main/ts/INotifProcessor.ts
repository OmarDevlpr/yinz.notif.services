import Notif from "@yinz/notif.data/models/Notif";

export default interface INotifProcessor {


    // prepareDate();
    process(notif: Notif, message: string, subject: string): Promise<void>;

}