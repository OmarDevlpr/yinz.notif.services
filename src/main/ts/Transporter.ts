// ext deps
import * as zmq from "zeromq";
import * as _ from "lodash";
// yinz deps
import { EventEmitter } from "events";
import {YinzOptions, Logger, Exception} from "@yinz/commons";

const DATATYPES = ["str", "obj", "bin"]

export type TransporterMode = "same-proc" | "same-host" | "diff-host" | "noop";

export interface TransporterMessage {
    encoding: "str" | "obj" | "bin";
    content: any;
}

export interface TransporterOptions {
    params?: {
        host?: string;
        port?: number;
        path?: string;
        mode: TransporterMode;
    }    
    logger: Logger;
}

export default class Transporter extends EventEmitter {


    private _producer: zmq.Socket;
    private _consumer: zmq.Socket;
    private _mode: TransporterMode;
    private _logger: Logger;

    private _path: string;
    private _host: string;
    private _port: number;

    private _uri: string;

    constructor(options: TransporterOptions) {
        
        super();

        this._mode = options.params && options.params.mode || "noop";
        this._logger = options.logger;

        this._logger.info(this._mode)

        // init tranporter params
        this.initModeParams(this._mode, options);

        // install consumer when required
        
        this.once("newListener", (event, listener) => {
            if ( ! this._consumer && event === "message") {
                this._logger.info("Will install the consumer");
                this.installConsumer(options);
            }
        })

    }

    private initModeParams(mode: TransporterMode, options: YinzOptions & TransporterOptions): void {

        switch ( mode ) {
          
            case "same-proc" : {
                this._path = options.params && options.params.path || "yinz";
                this._logger.info(`Will use path [${this._path}] to transport data`)  ;
                break;
            }

            case "same-host": {
                this._path = options.params && options.params.path || "tmp/yinz-transporter";
                this._logger.info(`Will use path [${this._path}] to transport data`);
                break;
            }

            case "diff-host": {
                this._host = options.params && options.params.host || "127.0.0.1";
                this._port = options.params && options.params.port || 12345;
                this._logger.info(`Will use host [${this._host}] to transport data`);
                this._logger.info(`Will use port [${this._port}] to transport data`);
                break;
            }

            case "noop": {
                this._logger.info("Will do nothing as the the transporter's mode is set to noop!")
                break;
            };

            default: {
                this._logger.warn("Transporter mode not specified... Will fallback to noop!")
                mode = "noop";
                break;
            };

        }
            

    }

    public send(message: any, options?: YinzOptions) {

        if (this._mode === 'noop') {
            this._logger.info("Will not send the supplied message as the transporter\'s mode is set to \'noop\'!");
            return;
        }

        // instantiate producer if required
        this.instnatiateProducer();

        // serialize data 
        let data = {} as TransporterMessage;

        if ( _.isString(message) ) {
            data.encoding = "str";
            data.content = new Buffer(message, "utf8");
        }
        else if (_.isPlainObject(message)) {
            data.encoding = "obj";
            data.content = new Buffer(JSON.stringify(message), "utf8");
        }
        else if (_.isBuffer(message)) {
            data.encoding = 'bin';
            data.content = message;
        }
        else {
            throw new Exception('ERR__TRANSPORTER__UNEXP_DATATYPE', {
                message: `Unexpected type of message being sent! Supported types are ${DATATYPES}`
            });
        }

        // convert the buffer into an array
        data.content = Array.prototype.slice.call(data.content, 0);


        // send the message
        this._logger.info('Will send a message...');
        this._producer.send(JSON.stringify(data));
        // this._logger.info('sent message [%s]', JSON.stringify(message, null, 2));

    }

    public release() {
        if (this._producer) {
            this._logger.trace('Will release producer...');
            this._producer.close();
        }
        if (this._consumer) {
            this._logger.trace('Will release consumer...');
            // this._consumer.removeAllListeners();
            this._consumer.close();
        }
    }

    private installConsumer(options: YinzOptions & TransporterOptions) {

        this.instantiateConsumer();

        this._logger.info(`will wait for messages to arrive on [${this._uri}]...`)

        this._consumer.on("message", (message: any) => {

            try {
                message = JSON.parse(message);
            } catch (e) {
                this._logger.error(`Transporter: Unexpected message : [${e.stack || e.message}]`)
            }

            if (! _.isArray(message.content)) {
                throw new Exception('ERR__TRANSPORTER__UNEXP_DATATYPE', {
                    message: `Unexpected type of received message! The message is expected to be a Buffer!`,
                    messageEncoding: message.encoding,
                    messageContent: message.content,
                });
            }

            // convert the content into a buffer
            message.content = new Buffer(message.content);  


            // deserialize the message
            if (message.encoding === 'str') {
                message = message.content.toString('utf8');
            }
            else if (message.encoding === 'obj') {
                message = JSON.parse(message.content.toString('utf8'));
            }
            else if (message.encoding !== 'bin') {
                throw new Exception('ERR__TRANSPORTER__UNEXP_DATATYPE', {
                    message: `Unexpected type of received message! Supported types are ${DATATYPES}`
                });
            }

            // this._logger.info('...received message [%s]...', JSON.stringify(message, null, 2));


            if (this.listenerCount("message") > 0) {
                this._logger.info('...the message will be dispatched immediatly.');
                this.emit("message", message);
            }
            else {

                this._logger.info('...will wait a listener before dispatching the message.');

                this.once("newListener", (event, listener) => {

                    this._logger.info('...will schedule the dispatch of the message for the next tick.');
                    process.nextTick(() => {
                        this._logger.info('...will dispatch the message as there is a listener now.');
                        this.emit('message', message);
                    });
                })
            }
        })        
    }

    private instantiateConsumer() {
        
        if (  this._consumer ) {
            return ;
        }

        switch ( this._mode ) {

            case "same-proc": {
                this._uri = "inproc://" + this._path;                
                break;
            }

            case "same-host": {
                this._uri = "ipc://" + this._path;
                break;
            }

            case "diff-host": {
                this._uri = "tcp://" + this._host + ":" + this._port;
                break;
            }

            default: {
                throw new Exception("ERR__TRANSPORTER__UNEXP_MODE", {
                    message: `Unexpected mode [${this._mode}]`
                })                
            }
        }
        
        this._logger.info(`About to instantiate a new consumer for uri [${this._uri}]...`)
        this._consumer = zmq.socket("pull");
        this._consumer.bindSync(this._uri);        
        
    }

    private instnatiateProducer() {

        if (this._producer) {
            return;
        }

        switch (this._mode) {

            case "same-proc": {
                this._uri = "inproc://" + this._path;
                break;
            }

            case "same-host": {
                this._uri = "ipc://" + this._path;
                break;
            }

            case "diff-host": {
                this._uri = "tcp://" + this._host + ":" + this._port;
                break;
            }

            default: {
                throw new Exception("ERR__TRANSPORTER__UNEXP_MODE", {
                    message: `Unexpected mode [${this._mode}]`
                })
            }
        }

        this._logger.info(`About to instantiate a new producer for uri [${this._uri}]...`)
        this._producer = zmq.socket("push");
        this._producer.connect(this._uri);

    }

}