import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import Transporter from "./Transporter";
import NotifManager from "./NotifManager";
import { DataSource, TypeormDao } from "@yinz/commons.data";
import Notif from "@yinz/notif.data/models/Notif";
import TemplEngine from "./TemplEngine";
import Templ from "@yinz/notif.data/models/Templ";
import EmailNotifProcessor from "./EmailNotifProcessor";



const registerBeans = (container: Container) => {

    let logger = container.getBean<Logger>("logger");

    let templEngine = new TemplEngine({ logger, });
    

    let transporter = new Transporter({
        logger,
        params: container.getParam("transporter"),
    })
  
    let notifManager = new NotifManager({
        logger,
        transporter,
        templEngine,
        dataSource: container.getBean<DataSource>('typeormDataSource'),
        notifDao: container.getBean<TypeormDao<Notif>>('notifDao'),
        archNotifDao: container.getBean<TypeormDao<Notif>>('archNotifDao'),        
        templDao: container.getBean<TypeormDao<Templ>>('templDao')
    })
    
    logger.warn('->', container.getParams())

    let emailNotifProcessor = new EmailNotifProcessor({
        logger,
        notifManager,
        params: container.getParam('email')
    })

    container.setBean('templEngine', templEngine);    
    container.setBean('transporter', transporter);        
    container.setBean('emailNotifProcessor', emailNotifProcessor);    
    container.setBean('notifManager', notifManager);

};

const registerClazzes = (container: Container) => {

    container.setClazz('TemplEngine', TemplEngine);
    container.setClazz('Transporter', Transporter);    
    container.setClazz('EmailNotifProcessor', EmailNotifProcessor);
    container.setClazz('NotifManager', NotifManager);
    
};


export {
    registerBeans,
    registerClazzes
};