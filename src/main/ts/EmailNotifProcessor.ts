import * as nodemailer from "nodemailer"
import * as smtpPool from "nodemailer-smtp-pool";

import INotifProcessor from "./INotifProcessor";
import Notif from "@yinz/notif.data/models/Notif";
import { Logger } from "@yinz/commons"
import NotifManager from "./NotifManager";

export interface EmailNotifProcessorOptions {
    logger: Logger
    notifManager?: NotifManager,
    params: TransportOptions & any
}

export interface TransportOptions {
    port: number;
    host: string;
    ignoreTLS: boolean;
    name: string;
    authMethod: string;
    auth: any;
    maxConnections: number;
    maxMessages: number;
    debug: boolean;		
    from: string;	    
}

export default class EmailNotifProcessor implements INotifProcessor {

    private logger: Logger;
    private mailer: nodemailer.Transporter;
    private transportOptions: TransportOptions;
    private from: string;
    // private notifManager: NotifManager;

    constructor(options: EmailNotifProcessorOptions) {

        let params = options.params;

        this.logger = options.logger;

        this.transportOptions = {} as TransportOptions;
        this.transportOptions.port = params.port;
        this.transportOptions.host = params.host;
        this.transportOptions.ignoreTLS = params.ignoreTLS;
        this.transportOptions.name = params.name;
        this.transportOptions.authMethod = params.authMethod;
        this.transportOptions.auth = params.auth;
        this.transportOptions.maxConnections = params.maxConnections || 5; // default
        this.transportOptions.maxMessages = params.maxMessages || 100; // default
        this.transportOptions.debug = params.debug || true;

        this.from = params.from;


        // subscribe processor to manager
        if ( options && options.notifManager ) {
            options.notifManager.subscribe('E', this);
        }

    }

    private instantiateMailer(transportOptions): void {

        if ( !this.mailer ) {
            this.logger.info('instantiating mailer...')
            let transport = smtpPool(transportOptions)
            this.mailer = nodemailer.createTransport(transport);                                
        }        

    }

    public async process(notif: Notif, message: string, subject?: string): Promise<any> {

        this.logger.debug('Has received for processing the notification ' + JSON.stringify(notif) + ' with message ' + message);

        subject = subject || notif.subject;
        this.instantiateMailer(this.transportOptions);

        let mailOptions = {
            from: this.from,
            to: notif.dest,
            subject: subject,
            html: message,
        };

        let info = await this.mailer.sendMail(mailOptions);

        return info;        
    }

}